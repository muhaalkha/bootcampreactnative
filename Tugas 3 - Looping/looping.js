// No. 1 Looping While
console.log("\nNo. 1 Looping While");
var myString_1 = "I Love Coding";
var myString_2 = "I will become a mobile developer";
var iteration_1 = 1;
var iteration_2 = 20;

console.log("LOOPING PERTAMA");

while (iteration_1 < 21) {
  if (iteration_1 % 2 == 0) {
    console.log(iteration_1 + " - " + myString_1);
  }
  iteration_1++;
}

console.log("LOOPING KEDUA");

while (iteration_2 > 0) {
  if (iteration_2 % 2 == 0) {
    console.log(iteration_2 + " - " + myString_2);
  }
  iteration_2--;
}

// No. 2 Looping menggunakan for
console.log("\nNo. 2 Looping menggunakan for");
var santai = "Santai";
var berkualitas = "Berkualitas";
var iLoveCoding = "I Love Coding";

for (let i = 1; i < 21; i++) {
  if (i % 3 == 0 && i % 2 != 0) {
    console.log(i + " - " + iLoveCoding);
  } else if (i % 2 != 0) {
    console.log(i + " - " + santai);
  } else if (i % 2 == 0) {
    console.log(i + " - " + berkualitas);
  }
}

// No. 3 Membuat Persegi Panjang #
console.log("\nNo. 3 Membuat Persegi Panjang #");
for (let i = 1; i <= 4; i++) {
  for (let j = 1; j <= 8; j++) {
    process.stdout.write("#");
  }
  process.stdout.write("\n");
}

// No. 4 Membuat Tangga
console.log("\nNo. 4 Membuat Tangga");
for (let i = 1; i <= 7; i++) {
  for (let j = 1; j <= i; j++) {
    process.stdout.write("#");
  }
  process.stdout.write("\n");
}

// No. 5 Membuat Papan Catur
console.log("\nNo. 5 Membuat Papan Catur");

for (let i = 1; i <= 8; i++) {
  if (i % 2 !== 0) {
    for (let j = 1; j <= 4; j++) {
      process.stdout.write(" #");
    }
  } else if (i % 2 === 0) {
    for (let k = 1; k <= 4; k++) {
      process.stdout.write("# ");
    }
  }
  process.stdout.write("\n");
}
