import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";

import AboutScreen from "../Pages/AboutScreen";
import AddScreen from "../Pages/AddScreen";
import HomeScreen from "../Pages/Home";
import LoginScreen from "../Pages/Login";
import ProjectScreen from "../Pages/ProjectScreen";
import Setting from "../Pages/Setting";
import SkillProject from "../Pages/SkillProject";
import Home from "../Pages/Home";

const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function Router() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Login Screen" component={LoginScreen} />
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="MainApp" component={MainApp} />
        <Stack.Screen name="MyDrawwer" component={MyDrawwer} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const MainApp = () => (
  <Tab.Navigator>
    <Tab.Screen name="HomeScreen" component={HomeScreen} />
    <Tab.Screen name="AddScreen" component={AddScreen} />
    <Tab.Screen name="SkillProject" component={SkillProject} />
  </Tab.Navigator>
);

const MyDrawwer = () => (
  <Drawer.Navigator>
    <Drawer.Screen name="App" component={MainApp} />
    <Drawer.Screen name="AboutScreen" component={AboutScreen} />
  </Drawer.Navigator>
);
