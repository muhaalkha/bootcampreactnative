import React, { useEffect } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  ScrollView,
} from "react-native";
export default function AboutScreen() {
  useEffect(() => {}, []);

  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.cardAbout}>
          <Text style={styles.headerText}>Tentang Saya</Text>
          <Image style={styles.avatar} source={require("./asset/avatar.png")} />
          <Text style={styles.name}>Muhammad Habib Al Khairi</Text>
          <Text style={styles.reactText}>React Native Developer</Text>
        </View>
        <View style={styles.cardPorto}>
          <Text style={styles.headerText}>Portofolio</Text>
          <View style={styles.iconPosition}>
            <View style={{ marginHorizontal: 30, alignItems: "center" }}>
              <Image
                style={styles.icon}
                source={require("./asset/gitlab.png")}
              />
              <Text style={styles.username}>@muhaalkha</Text>
            </View>
            <View style={{ marginHorizontal: 30, alignItems: "center" }}>
              <Image
                style={styles.icon}
                source={require("./asset/github.png")}
              />
              <Text style={styles.username}>@muhaalkha</Text>
            </View>
          </View>
        </View>
        <View style={styles.cardContact}>
          <Text style={styles.headerText}>Kontak</Text>
          <View style={styles.iconPosition}>
            <View style={{ marginHorizontal: 30, alignItems: "center" }}>
              <Image
                style={styles.icon}
                source={require("./asset/whatsapp.png")}
              />
              <Text style={styles.username}>@muhaalkha</Text>
            </View>
            <View style={{ marginHorizontal: 30, alignItems: "center" }}>
              <Image
                style={styles.icon}
                source={require("./asset/linkedin.png")}
              />
              <Text style={styles.username}>@muhaalkha</Text>
            </View>
          </View>
          <View style={styles.iconPosition}>
            <View style={{ marginHorizontal: 30, alignItems: "center" }}>
              <Image
                style={styles.icon}
                source={require("./asset/gmail.png")}
              />
              <Text style={styles.username}>@muhaalkha</Text>
            </View>
            <View style={{ marginHorizontal: 30, alignItems: "center" }}>
              <Image style={styles.icon} source={require("./asset/tele.png")} />
              <Text style={styles.username}>@muhaalkha</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Keluar</Text>
          </View>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FEFEFE",
    alignItems: "center",
  },
  scrollView: {
    backgroundColor: "pink",
    //marginHorizontal: 20,
  },
  cardAbout: {
    backgroundColor: "white",
    height: 340,
    width: 290,
    elevation: 25,
    borderRadius: 25,
    alignItems: "center",
    marginTop: 56,
  },
  headerText: {
    color: "#003366",
    fontSize: 22,
    //fontFamily: 'sans-serif-condensed',
    fontWeight: "700",
    marginTop: 30,
  },
  avatar: {
    marginTop: 30,
  },
  name: {
    color: "#003366",
    marginTop: 30,
    fontSize: 16,
    fontWeight: "bold",
  },
  reactText: {
    color: "#003366",
    fontSize: 14,
    marginTop: 5,
    fontFamily: "sans-serif-light",
  },
  cardPorto: {
    backgroundColor: "white",
    height: 200,
    width: 290,
    elevation: 25,
    borderRadius: 25,
    alignItems: "center",
    marginTop: 56,
  },
  iconPosition: {
    marginTop: 30,
    flexDirection: "row",
  },
  username: {
    color: "#2a2a2a",
    marginTop: 5,
    fontSize: 14,
    fontWeight: "bold",
  },
  cardContact: {
    backgroundColor: "white",
    height: 300,
    width: 290,
    elevation: 25,
    borderRadius: 25,
    alignItems: "center",
    marginTop: 56,
  },
  button: {
    backgroundColor: "#003366",
    height: 34,
    width: 217,
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 56,
    marginBottom: 56,
  },
  buttonText: {
    textAlign: "center",
    color: "white",
    fontWeight: "bold",
  },
});
