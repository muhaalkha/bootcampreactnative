import React, { useEffect } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
} from "react-native";
export default function LoginScreen() {
  useEffect(() => {}, []);

  return (
    <View style={styles.container}>
      <View style={{ alignItems: "center" }}>
        <Image style={styles.logo} source={require("./asset/logo_2.png")} />
      </View>
      <View style={styles.loginContainer}>
        <Text style={styles.masukText}>Selamat Datang</Text>
        <View style={styles.username}>
          <Text style={{ textAlign: "left", marginBottom: 7, opacity: 0.3 }}>
            Username
          </Text>
        </View>
        <View style={styles.password}>
          <Text style={{ textAlign: "left", marginBottom: 7, opacity: 0.3 }}>
            Password
          </Text>
        </View>
        <TouchableOpacity style={{ marginTop: 40 }}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Masuk</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.belumDaftarContainer}>
          <Text style={styles.belumDaftar}>Belum punya akun?</Text>
          <TouchableOpacity>
            <Text
              style={{ color: "#003366", fontWeight: "bold", marginTop: 30 }}
            >
              {" "}
              Daftar
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FEFEFE",
    alignItems: "center",
  },
  logo: {
    width: 267,
    height: 78,
    marginTop: 56,
  },
  loginContainer: {
    marginTop: 56,
    backgroundColor: "white",
    height: 340,
    width: 290,
    elevation: 25,
    borderRadius: 25,
    alignItems: "center",
  },
  masukText: {
    marginTop: 30,
    color: "#003366",
    fontSize: 22,
    //fontFamily: 'sans-serif-condensed',
    fontWeight: "700",
  },
  username: {
    marginTop: 30,
    backgroundColor: "white",
    height: 28,
    width: 217,
    borderBottomWidth: 1,
    borderBottomColor: "#003366",
  },
  password: {
    marginTop: 40,
    backgroundColor: "white",
    height: 28,
    width: 217,
    borderBottomWidth: 1,
    borderBottomColor: "#003366",
  },
  button: {
    backgroundColor: "#003366",
    height: 34,
    width: 217,
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    textAlign: "center",
    color: "white",
    fontWeight: "bold",
  },
  belumDaftar: {
    color: "#003366",
    fontFamily: "sans-serif-light",
    marginTop: 30,
  },
  belumDaftarContainer: {
    flex: 1,
    flexDirection: "row",
  },
});
