import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Router from "./Quiz3/index";

export default function App() {
  return <Router />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: "white",
    alignItems: "center",
    justifyContent: "center",
  },
});
