import React, { useEffect } from "react";
import { useState } from "react";
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { Data } from "./data";

export default function Home({ route, navigation }) {
  const { username } = route.params;
  const [totalPrice, setTotalPrice] = useState(0);

  const currencyFormat = (num) => {
    return "Rp " + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  };

  const updateHarga = (price) => {
    console.log("UpdatPrice : " + price);
    const temp = Number(price) + totalPrice;
    console.log(temp);
    setTotalPrice(temp);

    //? #Bonus (10 poin) -- HomeScreen.js --
    //? agar harga dapat update misal di tambah lebih dari 1 item atau lebih -->
  };
  return (
    <View style={styles.container}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          padding: 16,
        }}
      >
        <View style={{ flexDirection: "column" }}>
          <Text>Selamat Datang,</Text>
          <Text style={{ fontSize: 18, fontWeight: "bold" }}>{username}</Text>
        </View>
        <View>
          <Text>Total Harga:</Text>
          <Text style={{ fontSize: 18, fontWeight: "bold" }}>
            {" "}
            {currencyFormat(totalPrice)}
          </Text>
        </View>
      </View>
      <View style={{ alignItems: "center" }}>
        <ScrollView>
          <SafeAreaView>
            <FlatList
              style={{
                backgroundColor: "white",
                marginBottom: 80,
              }}
              numColumns={2}
              data={Data}
              keyExtractor={(item) => item.id}
              renderItem={({ item }) => {
                return (
                  <>
                    <View
                      style={{
                        height: 250,
                        width: 150,
                        borderWidth: 1,
                        borderRadius: 8,
                        margin: 10,
                        padding: 5,
                        alignItems: "center",
                      }}
                    >
                      <Text style={{ fontWeight: "600" }}>{item.title}</Text>
                      <Image
                        style={{
                          borderRadius: 8,
                          marginTop: 5,
                          width: 120,
                          height: 120,
                        }}
                        source={item.image}
                      />
                      <Text style={{ marginTop: 5, fontWeight: "600" }}>
                        {item.harga}
                      </Text>
                      <Text style={{ marginVertical: 5, fontWeight: "600" }}>
                        {item.type}
                      </Text>
                      <Button
                        style={{ marginTop: 5 }}
                        title="   BELI   "
                        onPress={() => updateHarga(item.harga)}
                      />
                    </View>
                  </>
                );
              }}
            />
          </SafeAreaView>
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  content: {
    width: 150,
    height: 220,
    margin: 5,
    borderWidth: 1,
    alignItems: "center",
    borderRadius: 5,
    borderColor: "grey",
  },
});
