// Soal No. 1 (Range)
console.log("Soal No. 1 (Range) ");

function range(startNum, finishNum) {
  var ourArray = [];
  if (typeof startNum == "undefined" || typeof finishNum == "undefined") {
    return -1;
  } else if (startNum > finishNum) {
    for (let i = startNum; i >= finishNum; i--) {
      ourArray.push(i);
    }
    return ourArray;
  } else if (startNum < finishNum) {
    for (let i = startNum; i <= finishNum; i++) {
      ourArray.push(i);
    }
    return ourArray;
  }
}
console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

// Soal No. 2 (Range with Step)
console.log("\nSoal No. 2 (Range with Step)");

function rangeWithStep(startNum, finishNum, step) {
  var ourArray = [];
  if (startNum > finishNum) {
    for (let i = startNum; i >= finishNum; i -= step) {
      ourArray.push(i);
    }
    return ourArray;
  } else if (startNum < finishNum) {
    for (let i = startNum; i <= finishNum; i += step) {
      ourArray.push(i);
    }
    return ourArray;
  }
}
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

// Soal No. 3 (Sum of Range)
console.log("\nSoal No. 3 (Sum of Range)");
function sum(startNum, finishNum, step = 1) {
  var ourSum = 0;
  if (typeof startNum == "undefined" && typeof finishNum == "undefined") {
    return 0;
  } else if (typeof finishNum == "undefined") {
    return startNum;
  } else if (startNum > finishNum) {
    for (let i = startNum; i >= finishNum; i -= step) {
      ourSum += i;
    }
    return ourSum;
  } else if (startNum < finishNum) {
    for (let i = startNum; i <= finishNum; i += step) {
      ourSum += i;
    }
    return ourSum;
  }
}
console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

//Soal No. 4 (Array Multidimensi)
console.log("\nSoal No. 4 (Array Multidimensi)");

var input1 = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

function dataHandling(ourArray) {
  var ourArrayLength = ourArray.length;
  for (let i = 0; i <= ourArrayLength - 1; i++) {
    console.log("Nomor ID: " + ourArray[i][0]);
    console.log("Nama Lengkap: " + ourArray[i][1]);
    console.log("TTL: " + ourArray[i][2] + " " + ourArray[i][3]);
    console.log("Hobi: " + ourArray[i][4] + "\n");
  }
}

dataHandling(input1);

// Soal No. 5 (Balik Kata)
console.log("\nSoal No. 5 (Balik Kata)");

function balikKata(ourString) {
  var ourStringLength = ourString.length;
  var stringBuffer = "";
  for (let i = ourStringLength - 1; i >= 0; i--) {
    stringBuffer += ourString[i];
  }
  return stringBuffer;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I

// Soal No. 6 (Metode Array)
console.log("\nSoal No. 6 (Metode Array)");

function dataHandling2(ourArray) {
  var modArray = ourArray;
  modArray.splice(
    1,
    4,
    "Roman Alamsyah Elsharawy",
    "Provinsi Bandar Lampung",
    "21/05/1989",
    "Pria",
    "SMA Internasional Metro"
  );
  console.log(modArray);
  var date = modArray[3].split("/");
  var month = Number(date[1]);
  switch (month) {
    case 1: {
      console.log("Januari");
      break;
    }
    case 2: {
      console.log("Februari");
      break;
    }
    case 3: {
      console.log("Maret");
      break;
    }
    case 4: {
      console.log("April");
      break;
    }
    case 5: {
      console.log("Mei");
      break;
    }
    case 6: {
      console.log("Juni");
      break;
    }
    case 7: {
      console.log("Juli");
      break;
    }
    case 8: {
      console.log("Agustus");
      break;
    }
    case 9: {
      console.log("September");
      break;
    }
    case 10: {
      console.log("Oktober");
      break;
    }
    case 11: {
      console.log("November");
      break;
    }
    case 12: {
      console.log("Desember");
      break;
    }
  }
  var sortedDate = date.slice();
  sortedDate.sort(function (value1, value2) {
    return value2 - value1;
  });
  console.log(sortedDate);
  var joinedDate = date.join("-");
  console.log(joinedDate);
  var name = modArray[1];
  name = name.slice(0, 14);
  console.log(name);
}

var input2 = [
  "0001",
  "Roman Alamsyah ",
  "Bandar Lampung",
  "21/05/1989",
  "Membaca",
];
dataHandling2(input2);
