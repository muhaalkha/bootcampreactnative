let readBooksPromise = require("./promise.js");

let books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

function runPromise(waktu, buku) {
  readBooksPromise(waktu, buku[0])
    .then(function (fullfield) {
      readBooksPromise(fullfield, buku[1])
        .then(function (fullfield) {
          readBooksPromise(fullfield, buku[2])
            .then(function (fullfield) {
              readBooksPromise(fullfield, buku[0])
                .then(function (fullfield) {})
                .catch(function (error) {
                  console.log("Waktu saya habis");
                });
            })
            .catch(function (error) {
              console.log("Waktu saya habis");
            });
        })
        .catch(function (error) {
          console.log("Waktu saya habis");
        });
    })
    .catch(function (error) {
      console.log("Waktu saya habis");
    });
}

runPromise(10000, books);

// var i = 0;
// function runPromise_2(time, books) {
//   readBooksPromise(time, books[i])
//     .then(function (fullfield) {
//       i++;
//       runPromise_2(fullfield, books);
//     })
//     .catch(function (error) {
//       console.log("Waktu Habis");
//     });
// }
// runPromise_2(15000, books);
