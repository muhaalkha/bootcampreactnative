// Soal No. 1 (Array to Object)
console.log("Soal No. 1 (Array to Object)");

var now = new Date();
var thisYear = now.getFullYear();

function arrayToObject(arr) {
  if (arr.length != 0) {
    for (i = 0; i <= arr.length - 1; i++) {
      var newObject = {};
      newObject.firstName = arr[i][0];
      newObject.lastName = arr[i][1];
      newObject.gender = arr[i][2];
      if (typeof arr[i][3] === "undefined" || arr[i][3] > thisYear) {
        newObject.age = "Invalid birth year";
      } else {
        newObject.age = thisYear - arr[i][3];
      }
      var consoleText =
        i + 1 + ". " + newObject.firstName + " " + newObject.lastName + ": ";
      console.log(consoleText);
      console.log(newObject);
    }
  } else {
    console.log("");
  }
}

// Driver Code
var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);

// Error case
arrayToObject([]); // ""

// Soal No. 2 (Shopping Time)
console.log("\nSoal No. 2 (Shopping Time)");

function shoppingTime(memberId, money) {
  if (typeof memberId == "undefined" || memberId == "") {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else {
    if (money >= 50000) {
      var shoppingData = {};
      shoppingData.memberId = memberId;
      shoppingData.money = money;
      shoppingData.listPurchased = [];
      shoppingData.changeMoney = money;
      if (shoppingData.changeMoney >= 1500000) {
        shoppingData.listPurchased.push("Sepatu Stacattu");
        shoppingData.changeMoney -= 1500000;
      }
      if (shoppingData.changeMoney >= 500000) {
        shoppingData.listPurchased.push("Baju Zoro");
        shoppingData.changeMoney -= 500000;
      }
      if (shoppingData.changeMoney >= 250000) {
        shoppingData.listPurchased.push("Baju H&N");
        shoppingData.changeMoney -= 250000;
      }
      if (shoppingData.changeMoney >= 175000) {
        shoppingData.listPurchased.push("Sweater Uniklooh");
        shoppingData.changeMoney -= 175000;
      }
      if (shoppingData.changeMoney >= 50000) {
        shoppingData.listPurchased.push("Casing Handphone");
        shoppingData.changeMoney -= 50000;
      }
      return shoppingData;
    } else {
      return "Mohon maaf, uang tidak cukup";
    }
  }
}

console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime("", 2475000));
console.log(shoppingTime("234JdhweRxa53", 15000));
console.log(shoppingTime());

// Soal No. 3 (Naik Angkot)
console.log("\nSoal No. 3 (Naik Angkot)");

function naikAngkot(arrPenumpang) {
  var rute = ["A", "B", "C", "D", "E", "F"];
  if (arrPenumpang.length == 0) {
    console.log("[]");
  } else {
    for (i = 0; i <= arrPenumpang.length - 1; i++) {
      var dataPenumpang = {
        penumpang: arrPenumpang[i][0],
        naikDari: arrPenumpang[i][1],
        tujuan: arrPenumpang[i][2],
      };
      var bayar =
        (rute.indexOf(dataPenumpang.tujuan) -
          rute.indexOf(dataPenumpang.naikDari)) *
        2000;
      dataPenumpang.bayar = bayar;
      console.log(dataPenumpang);
    }
  }
}

//TEST CASE
naikAngkot([
  ["Dimitri", "B", "F"],
  ["Icha", "A", "B"],
]);

naikAngkot([]); //[]
