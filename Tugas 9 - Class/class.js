// 1. Animal Class
console.log("1. Animal Class ");
class Animal {
  constructor(name) {
    this.name = name;
    this.legs = 4;
    this.cold_blooded = false;
  }
  get gname() {
    return this.name;
  }
  get glegs() {
    return this.legs;
  }
  get gcold_blooded() {
    return this.cold_blooded;
  }
}

var sheep = new Animal("shaun");

console.log(sheep.gname); // "shaun"
console.log(sheep.glegs); // 4
console.log(sheep.gcold_blooded); // false

class Ape extends Animal {
  constructor(name) {
    super(name);
    this.legs = 2;
    this.cold_blooded = false;
    this.sound = "Auooo";
  }
  yell() {
    return console.log(this.sound);
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name);
    this.legs = 4;
    this.cold_blooded = false;
    this.sound = "hop hop";
  }
  jump() {
    return console.log(this.sound);
  }
}

var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"

var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"

// 2. Function to Class
console.log("\n2. Function to Class");
class Clock {
  constructor({ template }) {
    this.template = template;
  }

  render() {
    let date = new Date();

    let hours = date.getHours();
    if (hours < 10) hours = "0" + hours;

    let mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;

    let secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;

    let output = this.template
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);

    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}

var clock = new Clock({ template: "h:m:s" });
clock.start();
