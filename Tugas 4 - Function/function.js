// No. 1
console.log("Soal No. 1");
function teriak() {
  return "Halo Sanbers!";
}
console.log(teriak());

// No. 2
console.log("\nSoal No. 2");
function kalikan(num1, num2) {
  return num1 * num2;
}
var hasilKali = kalikan(4, 100);
console.log(hasilKali);

// No. 3
console.log("\nSoal No. 3");
function instroduce(name, age, adress, hobby) {
  let myString =
    "Nama saya " +
    name +
    ", umur saya " +
    age +
    " tahun, alamat saya di " +
    adress +
    ", dan saya punya hobby yaitu " +
    hobby +
    "!";
  return myString;
}

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
var perkenalan = instroduce(name, age, address, hobby);
console.log(perkenalan);
